#!/bin/bash
apt-get update
apt-get install apt-transport-https ca-certificates curl -y

apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo debian-jessie main" >>  /etc/apt/sources.list.d/docker.list
apt-get update
apt-cache policy docker-engine -y
apt-get install docker-engine -y
curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
git clone git://github.com/jpetazzo/orchestration-workshop
